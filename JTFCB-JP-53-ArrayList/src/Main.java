import java.awt.List;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		ArrayList<Integer> numbers = new ArrayList<Integer>();
		// Adding
		numbers.add(10);
		numbers.add(100);
		numbers.add(40);

		// Retrieving
		System.out.println(numbers.get(0));

		// Indexed for loop iteration
		for (int i = 0; i < numbers.size(); i++) {

			System.out.println(numbers.get(i));

			// Removing items (careful!)
			numbers.remove(numbers.size() - 1);

			// This is VERY slow
			numbers.remove(0);

			for (Integer value : numbers) {

				System.out.println(value);
			}

			// jak chcemy usuwac cos zeby bylo szybko to LinkedList

			// List interface [ kolekcje sa w domyslnym interfacie List
			// zgrupowane]
			// List <String> values = new ArrayList<String>();
		}
	}

}
